import { Game_Action } from "./Functions";
class Game {
  constructor(info){
    this._length = info.length
    this.pions = []
    this.teams = info.teams
    this.turn = 0
  }
  set turn (x) {
    let a;
    this._turn = x
    console.log("x: ",x);
    // console.log("%: ", this._turn % 2);
    a = this.teams['t'+ this._turn % 2]
    a.pions.forEach(x => x.pv ? x.pa = 1 : "" )
  }
  get turn () {
    return this._turn
  }
  action(cible, action){

	  let acteur = this.pion_actif.pion;
    if(!acteur.pa) return "pa";
    
    let checkTeam = (str) => {
      switch (str) {
        case "attack":
          if(cible.pion.team.pions.every(p => p.pv == 0)) {

            let win = this.turn
            if(acteur.team == cible.pion.team) win++;
            setTimeout(() => {
              alert(`victoire de la team ${this.teams['t'+ win%2].nom}`);
            }, 25);
            break;
          }
        case "move": if (acteur.team.pions.every(p => p.pa == 0)) this.turn = this.turn + 1;
          
          break;
      
        default:
          break;
      }
      
      return str;

    };

    acteur.pa = 0;

    return checkTeam(Game_Action[action]({target: cible, src: this.pion_actif}));
    
  }
  addEvent(){
    $('.case').click((e)=>{
      let trgt = e.currentTarget, action;
      if (!this.pion_actif && trgt.children.length) this.pion_actif = trgt.children[0];
      else {
        if (trgt.children.length) {
          trgt = trgt.children[0],
          action = "attack";
        } else action = "move";

        console.log(this.action(trgt, action));

        this.pion_actif = false;
      }
    })
    return "success"
  }

}

class Pion {
  constructor (info) {
    // super(info)
    // this.name = info.name
    this.pv = 1
    this.team = info
    this.pa = 0
  }
}

export {Game, Pion};