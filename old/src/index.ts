require("../assets/stylesheets/styles.scss");
import {Game, Pion} from "./Classes/Game.js"
var $ = require( "jquery" );

function init(){

  let teams:any = {
    t0: {
      pions :[],
      nom : 'bleu'
    },
    t1: {
      pions :[],
      nom : 'rouge'
    }
  };
  teams.t0.pions.push(new Pion(teams.t0));
  teams.t0.pions.push(new Pion(teams.t0));
  teams.t1.pions.push(new Pion(teams.t1));
  teams.t1.pions.push(new Pion(teams.t1));


  let partie = new Game({length:2, teams: teams});
  partie.teams.t1.pions.forEach((x: object) => partie.pions.push(x))
  partie.teams.t0.pions.forEach((x: object) => partie.pions.push(x))

  for (let i = 0; i<partie.pions.length; i++){
    let pion = document.createElement('div')
    let classes = ['pion', partie.pions[i].team.nom]
    pion.classList.add(...classes)
    pion.pion = partie.pions[i]
    $('#x'+(i+2)).append(pion)
  }
  console.log(partie.addEvent());
  return partie

}
const G = init()
 