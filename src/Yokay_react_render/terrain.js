import React from 'react';
import Partie from '../Yokai_source/partie'

class Case extends React.Component {
	render() {
		return (
			<div className="Case" onClick={() => this.props.onClick()} >
				{this.props.value}
			</div>
		);
	}
}

class Pion extends React.Component {
	render() { return <div className={"pion " + this.props.team} id={this.props.id}/> }
}

class Button extends React.Component {
	render() { return <button> {this.props.value} </button>}
}
class ActionWindow extends React.Component {

  renderItem(){ return this.props.items.map( (x, i) => x.render(x,i)) }

	render() { return (
		<div className="action_window">
			{this.renderItem()}
		</div>
	)}
}

export default class Terrain extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			plateau: Array(9).fill(null),
      partie: new Partie()
		}
	}

	bleu  = [0,1,2,3]
	rouge = [4,5,6,7]

	renderCase(i)   { return <Case value={this.state.plateau[i]} onClick={()=>this.action(i)}/> }
	renderPion(i,j) { return <Pion team={i} id={j} /> }

	renderButton(props, key){ return <Button value={ props.value } key={key}/> }

	renderActionWindow(actions){
    if(!actions) return;
		if (this.state.partie.actif != null){
			const items = [];
			for(const action of actions) { items.push({ value: action, render: this.renderButton }) }

			return (<ActionWindow items={ items }/>)
		}
	}

	action(i){
		let plateau = this.state.plateau.slice()
		// case vide
		if (plateau[i] == null) {   
			// mouvement
			if (this.state.partie.actif != null){
				if (this[this.state.partie.tour].includes(i)){
					let intermediaire = this.state.partie.mouvement(i)
					plateau = intermediaire.map( x => {return x = x != null ? this.renderPion(x.equipe, x.nom) : null })
				}
			}
		}
		// Case pleine
		else{
			// attaque
			if (this.state.partie.actif != null) {				
				let intermediaire = this.state.partie.attaque(i)
				plateau = intermediaire.map( x => {return x = x != null ? this.renderPion(x.equipe, x.nom) : null })
			}
			// selection du pion actif
			else {
				this.state.partie.select_pion_actif(i)
			}
		}
		//mise à jour de l'état de la partie
		this.setState({plateau: plateau})

		//verification conditions de victoire
		
		if ( this.state.partie.joueur.some( x => x.equipe === 'bleu' && x.pv ) && this.state.partie.joueur.some(x => x.equipe === 'rouge' && x.pv ) ) return
		else if ( !this.state.partie.joueur.some( x => x.equipe === 'bleu' && x.pv ) ) alert(`Victoire de l'équipe Rouge`)
		else  alert(`Victoire de l'équipe Bleu`)
	}

  recommencer(){
    this.setState({
	  plateau : [
      	null, null, 
      	this.renderPion('bleu' , 'joueur1'), this.renderPion('bleu' , "joueur2"), 
      	this.renderPion('rouge', "joueur3"), this.renderPion('rouge', "joueur4"),
      	null,null],
	  partie : new Partie()
    })
  }



	render(){
		return (
			<div>
				{console.log("render")}
        <button onClick={()=> this.recommencer()}>Recommencer</button>
				<h1>Tour de l'équipe {this.state.partie.tour}</h1>
				<div className="board">
					<div className="side">
						<div className="row">
							{this.renderCase(0)}
							{this.renderCase(1)}
						</div>
						<div className="row">
							{this.renderCase(2)}
							{this.renderCase(3)}
						</div>
					</div>
					<div className="side">
						<div className="row">
							{this.renderCase(4)}
							{this.renderCase(5)}
						</div>
						<div className="row">
							{this.renderCase(6)}
							{this.renderCase(7)}
						</div>
					</div>
				</div>
				
				{this.renderActionWindow(['annuler','attaque','mouvement'])}
			</div>
		)
	}
}
