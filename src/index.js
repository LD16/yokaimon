import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Terrain from './Yokay_react_render/terrain'

//	========================================

ReactDOM.render(
	<Terrain />,
	document.getElementById('root')
);