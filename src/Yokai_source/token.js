export default class Token {
	constructor( nom, position, equipe ) {
		[this.position, this.nom, this.equipe] = [position, nom, equipe]

		this._pa = 1
		this._pv = 10
	}

	set pa(x) { 
		this._pa = x
		if (this.pv <= 0 ) this.position = -1
	}
	set pv(x) { 
		this._pv = x; 
		if (this.pv <= 0 ) {
			this.pa = 0
			this.position = -1
		} 
	}
	
	get pa() { return this.pv > 0 ? this._pa : 0 }
	get pv() { return this._pv  }
}