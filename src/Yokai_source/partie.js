import Token from './token'

export default class Partie {
	constructor(){
		this.actif = null
		this.tour = 'bleu'
		this.joueur = [
			new Token('joueur1', 2 , "bleu" ) , new Token('joueur2', 3 , "bleu" ) , 
			new Token('joueur3', 4 , "rouge") , new Token('joueur4', 5 , "rouge") , 
		]
		this.plateau = [
			null, null, 
			this.joueur[0],this.joueur[1],
			this.joueur[2],this.joueur[3],
			null,null
		]
	}

	changeTour(){
		this.tour = this.tour === "rouge" ? 'bleu'  : 'rouge'
		this.joueur.forEach( (elem) => { if (elem.equipe === this.tour && elem.pv > 0) elem.pa = 1 })
	}

	mouvement(destination){
		[ this.plateau[destination], this.plateau[this.actif] ] = [ this.plateau[this.actif], this.plateau[destination] ]

		// modifier la position dans l'objet token
		let joueur_actif = this.joueur[ this.joueur.findIndex( x => x.position === this.actif ) ]
		joueur_actif.position = destination
		joueur_actif.pa = 0 

		// terminer le tour si aucun des membres de l'éqipe n'a de pa
		if( !this.joueur.some(x =>  x.pa > 0 && x.equipe === this.tour) ) this.changeTour()

		this.actif = null

		return this.plateau
	}

	attaque(cible){
		let joueur_actif = this.joueur[ this.joueur.findIndex( x => x.position === this.actif ) ]
		let victime = this.joueur[ this.joueur.findIndex( x => x.position === cible )]

		// diminuer les pv de la victime dans l'objet token et le pa du joueur actif
		victime.pv = 0
		joueur_actif.pa = 0

		// terminer le tour si aucun des membres de l'éqipe n'a de pa
		if( !this.joueur.some(x =>  x.pa > 0 && x.equipe === this.tour) ) this.changeTour()
		
		// supprimer la cible du plateau
		this.plateau[cible] = null
		this.actif = null

		return this.plateau
	}

	select_pion_actif(cible){
		let joueur_actif = this.joueur[ this.joueur.findIndex( x => x.position === cible ) ]

		//le joueur sélectionné ne devient actif que si c'est le tour de son équipe et qu'il a des pa
		if ( joueur_actif.equipe === this.tour && joueur_actif.pa) this.actif = cible
	}

	annuler_pion_actif(){
		this.actif = null
		return 0
	}
	
}
